/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('joc')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
