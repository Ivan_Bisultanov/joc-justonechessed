(function() {
  'use strict';

  angular
    .module('joc', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'ngResource',
      'ui.router',
      'ui.bootstrap',
      'toastr',
      'ui.select',
      'perfect_scrollbar']);

})();
