(function() {
  'use strict';

  angular
    .module('joc')
    .directive('owlSlider', owlSlider);

  /** @ngInject */
  function owlSlider() {
    var directive = {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/components/owl-slider/owl-slider.html',
      link: function link(scope, element, attrs) {
        element.find('.owl-carousel').owlCarousel({
          loop:true,
          margin:0,
          responsiveClass:true,
          navText: ['', ''],
          nav: true,
          responsive:{
            0:{
              items:2
            },
            600:{
              items:3
            },
            740:{
              items:4
            },
            850:{
              items:5
            },
            1000:{
              items:6
            }
          }
        });
      }
    };

    return directive;

    /** @ngInject */
  }

})();
