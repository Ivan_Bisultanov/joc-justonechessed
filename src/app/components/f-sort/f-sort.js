(function() {
  'use strict';

  angular
    .module('joc')
    .directive('fSort', fSort);

  /** @ngInject */
  function fSort() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/f-sort/f-sort.html',
      replace: true,
      controller: FSortController,
      controllerAs: 'vmsort',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function FSortController() {
      var vmsort = this;

      vmsort.category = null;
      vmsort.categoryArr = [
        'Driving',
        'Cooking',
        'Consulting',
        'Assistance'
      ];

      vmsort.country = null;
      vmsort.countryArr = [
        'United States',
        'Spain',
        'Italy'
      ];

    }
  }

})();
