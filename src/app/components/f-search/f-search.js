(function() {
  'use strict';

  angular
    .module('joc')
    .directive('fSearch', fSearch);

  /** @ngInject */
  function fSearch() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/f-search/f-search.html',
      replace: true,
      controller: FSearchController,
      controllerAs: 'vmsearch',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function FSearchController() {
      var vmsearch = this;

      vmsearch.category = null;
      vmsearch.categoryArr = [
        'Driving',
        'Cooking',
        'Consulting',
        'Assistance'
      ];

      vmsearch.country = null;
      vmsearch.countryArr = [
        'United States',
        'Spain',
        'Italy'
      ];

    }
  }

})();
