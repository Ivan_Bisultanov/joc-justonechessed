(function() {
  'use strict';

  angular
    .module('joc')
    .directive('fSignUp', fSignUp);

  /** @ngInject */
  function fSignUp() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/f-sign-up/f-sign-up.html',
      replace: true,
      controller: FSignUpController,
      controllerAs: 'fsign',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function FSignUpController() {

      this.step = 1;

      this.city = null;
      this.cityArr = [
        'London',
        'Paris'
      ];
      this.state = null;
      this.stateArr = [
        'Florida',
        'Nevada'
      ];
      this.areas = null;
      this.areasArr = [
        'Florida1',
        'Florida2',
        'Florida3',
        'Florida',
        'Nevada'
      ];
      this.willing = null;
      this.willingArr = [
        'Florida',
        'Nevada'
      ];

    }
  }

})();
