(function() {
  'use strict';

  angular
    .module('joc')
    .directive('mSite', mSite);

  /** @ngInject */
  function mSite() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/m-site/m-site.html',
      replace: true
    };

    return directive;
  }

})();
