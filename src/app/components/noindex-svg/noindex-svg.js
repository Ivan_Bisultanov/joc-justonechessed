(function() {
  'use strict';

  angular
    .module('joc')
    .directive('noindexSvg', noindexSvg);

  /** @ngInject */
  function noindexSvg() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/noindex-svg/noindex-svg.html',
      replace: true
    };

    return directive;
  }

})();
