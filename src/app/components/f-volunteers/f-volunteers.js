(function() {
  'use strict';

  angular
    .module('joc')
    .directive('fVolunteers', fVolunteers);

  /** @ngInject */
  function fVolunteers() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/f-volunteers/f-volunteers.html',
      replace: true,
      controller: FVolunteersController,
      controllerAs: 'vmvolunteer',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function FVolunteersController() {
      var vmvolunteer = this;

      vmvolunteer.category = null;
      vmvolunteer.categoryArr = [
        'Driving',
        'Cooking',
        'Consulting',
        'Assistance'
      ];

    }
  }

})();
