(function() {
  'use strict';

  angular
    .module('joc')
    .directive('sHeader', sHeader);

  /** @ngInject */
  function sHeader() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/s-header/s-header.html',
      replace: true
    };

    return directive;
  }

})();
