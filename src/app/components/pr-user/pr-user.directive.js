(function() {
  'use strict';

  angular
    .module('joc')
    .directive('prUser', prUser);

  /** @ngInject */
  function prUser() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/pr-user/pr-user.html',
      replace: true
    };

    return directive;
  }

})();
