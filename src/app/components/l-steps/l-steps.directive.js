(function() {
  'use strict';

  angular
    .module('joc')
    .directive('lSteps', lSteps);

  /** @ngInject */
  function lSteps() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/l-steps/l-steps.html',
      replace: true,
      controller: LStepsController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function LStepsController() {


    }
  }

})();
