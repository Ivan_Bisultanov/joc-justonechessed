(function() {
  'use strict';

  angular
    .module('joc')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('registration', {
        url: '/registration',
        templateUrl: 'app/registration/registration.html',
        controller: 'SignUpController',
        controllerAs: 'signUp'
      })
      .state('actions', {
        url: '/actions',
        templateUrl: 'app/actions/actions.html',
        controller: 'ActionsController',
        controllerAs: 'actions'
      })
      .state('volunteers', {
        url: '/volunteers',
        templateUrl: 'app/volunteers/volunteers.html',
        controller: 'ActionsController',
        controllerAs: 'volunteers'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
